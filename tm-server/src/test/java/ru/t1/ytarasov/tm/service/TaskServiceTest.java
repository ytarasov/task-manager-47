package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.service.dto.ProjectServiceDTO;
import ru.t1.ytarasov.tm.service.dto.ProjectTaskServiceDTO;
import ru.t1.ytarasov.tm.service.dto.TaskServiceDTO;
import ru.t1.ytarasov.tm.service.dto.UserServiceDTO;

import java.util.List;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private static final String NEW_TASK_NAME = "new task";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "new task";

    @NotNull
    private static final String UPDATE_TASK_NAME = "update task";

    @NotNull
    private static final String UPDATE_TASK_DESCRIPTION = "update task";

    @NotNull
    private static final String CHANGE_STATUS_NAME = "test change status";

    @NotNull
    private static final String REMOVE_TASK_NAME = "remove";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private static final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private static final IProjectTaskServiceDTO projectTaskService = new ProjectTaskServiceDTO(projectService, taskService);

    @NotNull
    private static final IUserServiceDTO userService = new UserServiceDTO(connectionService, propertyService);

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void createUser() throws Exception {
        @NotNull final UserDTO user = userService.create("test26", "test2", "test2@test", Role.USUAL);
        userId = user.getId();
    }

    @AfterClass
    public static void removeUser() throws Exception {
        projectService.clear(userId);
        userService.clear();
    }

    @Before
    public void setup() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO("test task 1");
        task.setUserId(userId);
        @NotNull final TaskDTO task1 = new TaskDTO("test task 2");
        task1.setUserId(userId);
        @NotNull final TaskDTO task2 = new TaskDTO("test task 3");
        task2.setUserId(userId);
        taskService.add(task);
        taskService.add(task1);
        taskService.add(task2);
    }

    @After
    public void tearDown() throws Exception {
        taskService.clear();
    }

    @Test
    public void findAll() throws Exception {
        final int expectedSize = taskService.getSize().intValue();
        @NotNull final List<TaskDTO> tasks = taskService.findAll();
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithComparator() throws Exception {
        final int expectedSize = taskService.getSize().intValue();
        @Nullable final List<TaskDTO> tasks = taskService.findAll(Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(tasks);
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithSort() throws Exception {
        final int expectedSize = taskService.getSize().intValue();
        @Nullable final List<TaskDTO> tasks = taskService.findAll(Sort.BY_STATUS);
        Assert.assertNotNull(tasks);
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(""));
        @NotNull final TaskDTO task = new TaskDTO(NEW_TASK_NAME);
        taskService.add(task);
        @Nullable final TaskDTO task1 = taskService.findOneById(task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void getSize() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int currentSize = taskService.getSize().intValue();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void changeStatusById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, "", Status.IN_PROGRESS));
        @NotNull final TaskDTO task = new TaskDTO(CHANGE_STATUS_NAME);
        task.setUserId(userId);
        taskService.add(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, task.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById("", task.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusById(userId, task.getId(), null));
        @Nullable final TaskDTO task1 = taskService.changeTaskStatusById(userId, task.getId(), Status.COMPLETED);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(task1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateById() throws Exception {
        @NotNull final TaskDTO task = taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById("", task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, null, UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, "", UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, task.getId(), null, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, task.getId(), "", UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, ""));
        @NotNull final TaskDTO task1 = taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.remove(null));
        @NotNull final TaskDTO task = new TaskDTO(REMOVE_TASK_NAME);
        taskService.add(task);
        taskService.remove(task);
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(""));
        @NotNull final TaskDTO task = new TaskDTO(REMOVE_TASK_NAME);
        taskService.add(task);
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void findAllWithUserId() throws Exception {
        @Nullable final String nullUserId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(nullUserId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(""));
        final int expectedSize = taskService.getSize(userId) + 1;
        @NotNull final TaskDTO task = taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        final int currentSize = taskService.getSize().intValue();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithUserIdAndComparator() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(null, Sort.BY_CREATED.getComparator()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll("", Sort.BY_CREATED.getComparator()));
        final int expectedSize = taskService.getSize(userId) + 1;
        @NotNull final TaskDTO task = taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        @Nullable final List<TaskDTO> tasks = taskService.findAll(userId, Sort.BY_CREATED.getComparator());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(expectedSize, tasks.size());
    }

    @Test
    public void findOneByIdAndUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(userId, ""));
        @NotNull final TaskDTO task = taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(null, task.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById("", task.getId()));
        @Nullable final TaskDTO task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getName(), NEW_TASK_NAME);
    }

    @Test
    public void getSizeWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(null));
        final int sizeExpected = taskService.getSize(userId) + 1;
        taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        final int sizeCurrent = taskService.getSize(userId);
        Assert.assertEquals(sizeExpected, sizeCurrent);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO("test project");
        project.setId(project.getId());
        project.setUserId(userId);
        projectService.add(project);
        @Nullable final TaskDTO task = taskService.create(userId, "test bind to project", NEW_TASK_DESCRIPTION);
        Assert.assertNotNull(task);
        projectTaskService.bindTaskToProject(userId, task.getId(), project.getId());
        @Nullable final TaskDTO task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO("test project");
        project.setId(project.getId());
        project.setUserId(userId);
        projectService.add(project);
        @Nullable final TaskDTO task = taskService.create(userId, "test bind to project", NEW_TASK_DESCRIPTION);
        Assert.assertNotNull(task);
        projectTaskService.bindTaskToProject(userId, task.getId(), project.getId());
        @Nullable final TaskDTO task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void clear() throws Exception {
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize().intValue());
    }

    @Test
    public void clearWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(""));
        @Nullable final List<TaskDTO> tasks = taskService.findAll(userId);
        @NotNull final TaskDTO task = new TaskDTO("test clear 1");
        task.setUserId(userId);
        @NotNull final TaskDTO task1 = new TaskDTO("test clear 2");
        task.setUserId(userId);
        @NotNull final TaskDTO task2 = new TaskDTO("test clear 3");
        task.setUserId(userId);
        taskService.add(task);
        taskService.add(task1);
        taskService.add(task2);
        taskService.clear(userId);
        Assert.assertEquals(0, taskService.getSize(userId));
        Assert.assertNotNull(tasks);
        for (TaskDTO task3 : tasks) taskService.add(task3);
    }

}
