package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;
import ru.t1.ytarasov.tm.repository.dto.TaskRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.*;

public final class TaskServiceDTO extends AbstractUserOwnedServiceDTO<TaskDTO, TaskRepositoryDTO> implements ITaskServiceDTO {

    public TaskServiceDTO(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public @NotNull TaskDTO add(@Nullable TaskDTO task) throws Exception {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public @NotNull Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models) throws Exception {
        for (@NotNull final TaskDTO model : models) add(model);
        return models;
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @Nullable List<TaskDTO> tasks = new ArrayList<>();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO taskRepository = new TaskRepositoryDTO(entityManager);
            tasks = taskRepository.findAll();
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable List<TaskDTO> tasks = new ArrayList<>();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO taskRepository = new TaskRepositoryDTO(entityManager);
            tasks = taskRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        List<TaskDTO> tasks = new ArrayList<>();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            tasks = repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        List<TaskDTO> tasks = new ArrayList<>();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            tasks = repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        else return findAll(sort.getComparator());
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        else return findAll(userId, sort.getComparator());
    }

    @Override
    public @Nullable TaskDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        TaskDTO task;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            task = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public @Nullable TaskDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        TaskDTO task;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            task = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public Long getSize() throws Exception {
        Long size;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            size = repository.getSize();
        } finally {
            entityManager.close();
        }
        return size;
    }

    @Override
    public int getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int size;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            size = repository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return size;
    }

    @Override
    public @NotNull TaskDTO remove(@Nullable TaskDTO task) throws Exception {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public TaskDTO removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public @Nullable TaskDTO removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public @Nullable TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        return add(model);
    }

    @Nullable
    @Override
    public TaskDTO remove(@Nullable String userId, @Nullable TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        @Nullable final TaskDTO task = findOneById(userId, model.getId());
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Boolean existsById;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            existsById = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        Boolean existsById;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            existsById = repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Override
    public @NotNull List<TaskDTO> findAllTasksByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try  {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            return repository.findAllTasksByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public TaskDTO update(@NotNull final TaskDTO task) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        task.setUpdated(new Date());
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(@Nullable final String userId,
                                        @Nullable final String id,
                                        @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    public void clear() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
