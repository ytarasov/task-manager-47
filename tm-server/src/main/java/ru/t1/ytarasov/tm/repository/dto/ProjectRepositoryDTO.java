package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepositoryDTO
        extends AbstractUserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@NotNull Comparator comparator) {
        @NotNull final String jpql = String.format("SELECT p FROM %s m ORDER BY :sort", getTableName());
        return entityManager
                .createQuery(jpql,ProjectDTO.class)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@NotNull String userId, @NotNull Comparator comparator) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = userId ORDER BY :sort");
        return entityManager
                .createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getTableName());
        return entityManager
                .createQuery(jpql, ProjectDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :id",
                getTableName(), EntityConstantDTO.COLUMN_ID);
        return entityManager
                .createQuery(jpql, ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull String userId) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :id AND p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_ID, EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, ProjectDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    protected @NotNull String getTableName() {
        return EntityConstantDTO.TABLE_PROJECT;
    }

}
