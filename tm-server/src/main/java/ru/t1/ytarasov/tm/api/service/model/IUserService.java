package ru.t1.ytarasov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable final String email) throws Exception;

    @Nullable
    User removeByLogin(@Nullable final String login) throws Exception;

    void lockUser(@Nullable final String login) throws Exception;

    void unlockUser(@Nullable final String login) throws Exception;

    void changePassword(@Nullable final String id, @Nullable final String newPassword) throws Exception;

}
