package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.model.IProjectRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.model.IProjectService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.repository.model.ProjectRepository;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        @Nullable final List<Project> projects;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            projects = repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll();
        else return findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @Nullable final List<Project> projects;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            projects = repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        else return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }
    @NotNull
    @Override
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @Nullable
    @Override
    public List<Project> findAll() throws Exception {
        @Nullable List<Project> projects;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            projects = repository.findAll();
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Override
    public Long getSize() throws Exception {
        Long size;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            size = repository.getSize();
        } finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    public Project add(@Nullable Project model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Collection<Project> add(@Nullable Collection<Project> models) throws Exception {
        if (models == null || models.isEmpty()) throw new ProjectNotFoundException();
        for (@NotNull final Project model : models) add(model);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsById;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            existsById = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project project;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public void clear() throws Exception {
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project remove(@Nullable Project model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Project removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Override
    public void update(@Nullable Project model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        model.setUpdated(new Date());
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Project> projects = findAll(userId);
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        for (@NotNull final Project project : projects) remove(project);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsById;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            existsById = repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable List<Project> projects;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            projects = repository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Override
    public @Nullable Project findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project project;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public int getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int size;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            size = repository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return size;
    }

    @Nullable
    @Override
    public Project removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Nullable
    @Override
    public Project add(@Nullable String userId, @Nullable Project model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Project remove(@Nullable String userId, @Nullable Project model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final Project project = findOneById(userId, model.getId());
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

}
