package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.repository.model.IRepository;
import ru.t1.ytarasov.tm.comparator.CreatedComparator;
import ru.t1.ytarasov.tm.comparator.NameComparator;
import ru.t1.ytarasov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortedType(@NotNull final Comparator comparator) {
        if (comparator == NameComparator.INSTANCE) return DBConstant.COLUMN_NAME;
        else if (comparator == CreatedComparator.INSTANCE) return DBConstant.COLUMN_CREATED;
        else return DBConstant.COLUMN_STATUS;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void clear() {
        @Nullable final List<M> models = findAll();
        if (models == null || models.isEmpty()) return;
        for (@NotNull final M model : models) remove(model);
    }

    @Override
    public Long getSize() {
        @NotNull final String jpql = String.format("SELECT COUNT(m) FROM %s m", getTableName());
        return entityManager
                .createQuery(jpql, Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT COUNT(1) = 1 FROM %s m WHERE %s = :id",
                getTableName(), DBConstant.COLUMN_ID);
        return entityManager
                .createQuery(jpql, boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

}
