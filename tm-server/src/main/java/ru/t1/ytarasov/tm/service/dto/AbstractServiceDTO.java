package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.dto.IServiceDTO;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;
import ru.t1.ytarasov.tm.repository.dto.AbstractRepositoryDTO;

import javax.persistence.EntityManager;

public abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends AbstractRepositoryDTO<M>> implements IServiceDTO<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractServiceDTO(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
