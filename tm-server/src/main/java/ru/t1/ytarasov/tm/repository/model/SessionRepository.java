package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.ISessionRepository;
import ru.t1.ytarasov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getTableName());
        return entityManager
                .createQuery(jpql, Session.class)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :id",
                getTableName(), EntityConstant.COLUMN_ID);
        return entityManager
                .createQuery(jpql, Session.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :userId",
                getTableName(), EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :id AND s.%s = :userId",
                getTableName(), EntityConstant.COLUMN_ID, EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, Session.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return EntityConstant.TABLE_SESSION;
    }

}
