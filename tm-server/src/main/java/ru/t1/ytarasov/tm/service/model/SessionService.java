package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.model.ISessionRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.model.ISessionService;
import ru.t1.ytarasov.tm.exception.entity.SessionNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.model.Session;
import ru.t1.ytarasov.tm.repository.model.SessionRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public final class SessionService
        extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public @Nullable List<Session> findAll() throws Exception {
        List<Session> sessions;
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            sessions = repository.findAll();
        } finally {
            entityManager.close();
        }
        return sessions;
    }

    @Override
    public Long getSize() throws Exception {
        Long size;
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            size = repository.getSize();
        } finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    public Session add(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Collection<Session> add(@Nullable Collection<Session> models) throws Exception {
        if (models == null || models.isEmpty()) throw new SessionNotFoundException();
        for (@NotNull final Session model : models) add(model);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsById;
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            existsById = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Override
    public @Nullable Session findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session;
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            session = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        return session;
    }

    @Override
    public void clear() throws Exception {
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable Session remove(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public @Nullable Session removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }

    @Override
    public void update(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        model.setUpdated(new Date());
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Session> sessions = findAll(userId);
        if (sessions == null || sessions.isEmpty()) return;
        for (@NotNull final Session session : sessions) {
            remove(session);
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsById;
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            existsById = repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Override
    public @Nullable List<Session> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        List<Session> sessions;
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            sessions = repository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return sessions;
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session;
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            session = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return session;
    }

    @Override
    public int getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int size;
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            size = repository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return size;
    }

    @Override
    public @Nullable Session removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }

    @Override
    public @Nullable Session add(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new SessionNotFoundException();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public @Nullable Session remove(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new SessionNotFoundException();
        @Nullable final Session session = findOneById(userId, model.getId());
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }
}
