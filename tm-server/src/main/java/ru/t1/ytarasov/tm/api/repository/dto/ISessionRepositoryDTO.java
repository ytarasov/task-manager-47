package ru.t1.ytarasov.tm.api.repository.dto;

import ru.t1.ytarasov.tm.dto.model.SessionDTO;

public interface ISessionRepositoryDTO extends IUserOwnedRepositoryDTO<SessionDTO> {
}
