package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class AbstractProjectTaskModelDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(nullable = false)
    protected String name = "";

    @Column
    @NotNull
    protected String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    public AbstractProjectTaskModelDTO(@NotNull String name) {
        this.name = name;
    }

    public AbstractProjectTaskModelDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public AbstractProjectTaskModelDTO(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    public AbstractProjectTaskModelDTO(@NotNull String name, @NotNull String description, @NotNull Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

}
