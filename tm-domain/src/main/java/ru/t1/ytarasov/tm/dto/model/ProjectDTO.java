package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.model.IWBS;
import ru.t1.ytarasov.tm.enumerated.Status;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProjectDTO extends AbstractProjectTaskModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    public ProjectDTO(@NotNull String name) {
        super(name);
    }

    public ProjectDTO(@NotNull String name, @NotNull String description) {
        super(name, description);
    }

    public ProjectDTO(@NotNull String name, @NotNull Status status) {
        super(name, status);
    }

    public ProjectDTO(@NotNull String name, @NotNull String description, @NotNull Status status) {
        super(name, description, status);
    }

    @Override
    public String toString() {
        return getId()+ " " + name + ": " + description + ": " + Status.toName(status);
    }

}
